-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 30 2017 г., 21:16
-- Версия сервера: 10.1.22-MariaDB
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `authentificator`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `surname`) VALUES
(1, 'admin', '123123', 'Roman', 'Olin');

-- --------------------------------------------------------

--
-- Структура таблицы `users_authentification`
--

CREATE TABLE `users_authentification` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `token` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users_authentification`
--

INSERT INTO `users_authentification` (`id`, `userId`, `date`, `token`, `active`) VALUES
(1, 1, '2017-03-30 20:41:44', '641d3f5599f06672ea5253c262f5c316', 0),
(2, 1, '2017-03-30 20:45:07', 'fa6eec7b76606c79cb1ed211cb86973e', 0),
(3, 1, '2017-03-30 20:45:17', '8fd62e58bca2d3f9c14687c68dc4f539', 0),
(4, 1, '2017-03-30 20:51:57', 'bea819d4546b865ed4b76ff75ee69666', 0),
(5, 1, '2017-03-30 20:55:12', '2defeadbd5d37683fbac360569460c8d', 0),
(6, 1, '2017-03-30 20:55:19', '53ba0c7fff75d9e088cb9fbb285804d0', 0),
(7, 1, '2017-03-30 21:08:11', '59edef3085a03fd33a1bcdbe77223109', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users_authentification`
--
ALTER TABLE `users_authentification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_authentification_users_authentification_id_fk` (`userId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users_authentification`
--
ALTER TABLE `users_authentification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users_authentification`
--
ALTER TABLE `users_authentification`
  ADD CONSTRAINT `users_authentification_users_authentification_id_fk` FOREIGN KEY (`userId`) REFERENCES `users_authentification` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
