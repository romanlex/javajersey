package com.authentificator.v1.dao;

import com.authentificator.core.database.Database;
import com.authentificator.core.exceptions.UsersAuthentificationsDAOException;
import com.authentificator.v1.pojo.User;
import com.authentificator.v1.pojo.UserAuthentification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UsersAuthentificationsDAO {

    private static Logger log = LoggerFactory.getLogger(UsersAuthentificationsDAO.class);

    private static final String INSERT_USER_AU = "INSERT INTO users_authentification (userId, date, token, active) VALUES (?, ?, ?, ?);";
    private static final String UPDATE_LAST_USER_AU = "UPDATE users_authentification SET active = ? WHERE userId = ? AND active = 1";
    private static final String UPDATE_USER_AU_BY_TOKEN = "UPDATE users_authentification SET active = 0 WHERE token = ? AND active = 1";

    public static long save(UserAuthentification ua) throws UsersAuthentificationsDAOException {
        Connection db = Database.pool.checkOut();
        long affectedRows = 0;

        try(PreparedStatement preparedStatement = db.prepareStatement(INSERT_USER_AU, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, ua.getUserId());
            preparedStatement.setTimestamp(2, ua.getDate());
            preparedStatement.setString(3, ua.getToken());
            preparedStatement.setInt(4, ua.getActive());
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UsersAuthentificationsDAOException(e.getMessage());
        }
        return affectedRows;
    }

    public static long inactiveLastAuth(User user) throws UsersAuthentificationsDAOException {
        Connection db = Database.pool.checkOut();
        long affectedRows = 0;

        try(PreparedStatement preparedStatement = db.prepareStatement(UPDATE_LAST_USER_AU, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1,0);
            preparedStatement.setInt(2, user.getId());
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UsersAuthentificationsDAOException(e.getMessage());
        }
        return affectedRows;
    }

    public static long inactiveAuth(String token) throws UsersAuthentificationsDAOException {
        Connection db = Database.pool.checkOut();
        long affectedRows = 0;

        try(PreparedStatement preparedStatement = db.prepareStatement(UPDATE_USER_AU_BY_TOKEN, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1,token);
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UsersAuthentificationsDAOException(e.getMessage());
        }
        return affectedRows;
    }
}



