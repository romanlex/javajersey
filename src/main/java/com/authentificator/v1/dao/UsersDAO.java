package com.authentificator.v1.dao;

import com.authentificator.core.database.Database;
import com.authentificator.core.exceptions.CoreException;
import com.authentificator.core.exceptions.ModelException;
import com.authentificator.core.exceptions.UserDAOException;
import com.authentificator.v1.pojo.User;
import com.authentificator.v1.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class UsersDAO {

    private static Logger log = LoggerFactory.getLogger(UsersDAO.class);

    private static final String FIND_USERS = "SELECT * FROM users ORDER BY id";
    private static final String FIND_USER_BY_EMAIL_SQL = "SELECT * FROM users WHERE login = ?";
    private static final String FIND_USER_BY_ID_SQL = "SELECT * FROM users WHERE id = ? LIMIT 1";

    public static User getUserByLogin(String username) throws UserDAOException, CoreException {
        User user = new User();
        user.setId(0);
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_EMAIL_SQL)) {
            ps.setString(1, username);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                UserService.bindToUser(user, result);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return user;
    }

    public static boolean userExist(String email) throws UserDAOException, CoreException {
        int rows = 0;
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_EMAIL_SQL)) {
            ps.setString(1, email);
            ResultSet result = ps.executeQuery();

            if (result.last()) {
                rows = result.getRow();
                result.beforeFirst();
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }

        return (rows > 0);
    }

    public static ResultSet getUsers() throws UserDAOException, ModelException {
        ResultSet result;
        Connection db = Database.pool.checkOut();

        try (Statement st = db.createStatement()) {
            result = st.executeQuery(FIND_USERS);
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return result;
    }


    public static ResultSet getUserById(int id) throws UserDAOException, ModelException {
        ResultSet result;
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_ID_SQL)) {
            ps.setInt(1, id);
            result = ps.executeQuery();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return result;
    }


}



