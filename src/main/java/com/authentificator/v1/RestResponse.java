package com.authentificator.v1;

import java.util.ArrayList;
import java.util.List;

public class RestResponse<T> {
    private String status = "404";
    private String message = "";
    private List<T> data = new ArrayList<T>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
