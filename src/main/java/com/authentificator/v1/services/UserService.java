package com.authentificator.v1.services;

import com.authentificator.core.exceptions.CoreException;
import com.authentificator.core.exceptions.ModelException;
import com.authentificator.core.exceptions.UserDAOException;
import com.authentificator.v1.dao.UsersDAO;
import com.authentificator.v1.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    public static User authenticated(String email, String password) throws UserDAOException, CoreException {
        if(email == null || password == null)
            throw new UserDAOException("Не указан логин или пароль");

        User user = UsersDAO.getUserByLogin(email);

        if(user.getId() == 0)
            throw new UserDAOException("Пользователь с данным email не найден");

        if(!password.equals(user.getPassword()))
            throw new UserDAOException("Пароль указан не верно");

        return user;
    }

    public static User getUserById(int id) throws UserDAOException {
        User user = new User();
        try {
            ResultSet rs = UsersDAO.getUserById(id);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            while (rs.next()) {
                bindToUser(user, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return user;
    }

    public static ArrayList<User> findAll() throws UserDAOException {
        ArrayList<User> userList = new ArrayList<>();
        try {
            ResultSet rs = UsersDAO.getUsers();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            while (rs.next()) {
                bindToUserList(userList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return userList;
    }

    public static void bindToUser(User user, ResultSet result) throws SQLException {
        user.setId(result.getInt("id"));
        user.setPassword(result.getString("password"));
        user.setLogin(result.getString("login"));
        user.setName(result.getString("name"));
        user.setSurname(result.getString("surname"));
    }

    public static void bindToUserList(ArrayList<User> userList, ResultSet result) throws SQLException {
        User user = new User();
        bindToUser(user, result);
        userList.add(user);
    }
}
