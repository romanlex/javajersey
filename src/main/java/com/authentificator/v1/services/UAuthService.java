package com.authentificator.v1.services;

import com.authentificator.core.exceptions.UsersAuthentificationsDAOException;
import com.authentificator.v1.dao.UsersAuthentificationsDAO;
import com.authentificator.v1.pojo.User;
import com.authentificator.v1.pojo.UserAuthentification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;


public class UAuthService {
    private static final Logger log = LoggerFactory.getLogger(UAuthService.class);

    public static long saveUserToken(String token, Timestamp date, User user) throws UsersAuthentificationsDAOException {
        setInactivePreviousAuth(user);
        UserAuthentification ua = new UserAuthentification();
        ua.setDate(date);
        ua.setUserId(user.getId());
        ua.setToken(token);
        ua.setActive(1);

        return UsersAuthentificationsDAO.save(ua);
    }

    public static long setInactivePreviousAuth(User user) throws UsersAuthentificationsDAOException {
        return UsersAuthentificationsDAO.inactiveLastAuth(user);
    }

    public static long setInactiveAuth(String token) throws UsersAuthentificationsDAOException {
        return UsersAuthentificationsDAO.inactiveAuth(token);
    }
}
