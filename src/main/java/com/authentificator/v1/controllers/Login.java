package com.authentificator.v1.controllers;

import com.authentificator.core.common.Utils;
import com.authentificator.core.exceptions.CoreException;
import com.authentificator.v1.RestResponse;
import com.authentificator.v1.pojo.User;
import com.authentificator.v1.services.UAuthService;
import com.authentificator.v1.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;

@Path("/login")
public class Login {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response login(@FormParam("username") String username,
                          @FormParam("password") String password) {
        RestResponse<User> resp = new RestResponse<>();
        try {
            // Authenticate the user using the credentials provided
            User user = authenticate(username, password);
            // Issue a token for the user
            String hash = issueToken(user);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String token = Utils.getMd5String(hash + timestamp + user.getLogin());

            if(UAuthService.saveUserToken(token, timestamp, user) == 0)
                throw new Exception("Cannot save user token to database");

            // Return the token on the response
            resp.setStatus("200");
            resp.setMessage(token);
            return Response.status(Response.Status.OK).entity(resp).build();

        } catch (Exception e) {
            resp.setStatus("401");
            resp.setMessage(e.getMessage());
            return Response.status(Response.Status.UNAUTHORIZED).entity(resp).build();
        }
    }


    private User authenticate(String username, String password) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        return UserService.authenticated(username, password);
    }

    private String issueToken(User user) throws CoreException {
        // Issue a token (can be a random String persisted to a database or a JWT token)
        // The issued token must be associated to a user
        // Return the issued token
        String hash = Utils.getMd5String(user.getLogin() + user.getPassword());
        return hash;
    }

}