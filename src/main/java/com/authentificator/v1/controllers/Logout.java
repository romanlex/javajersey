package com.authentificator.v1.controllers;

import com.authentificator.core.exceptions.UsersAuthentificationsDAOException;
import com.authentificator.v1.RestResponse;
import com.authentificator.v1.services.UAuthService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/logout")
public class Logout {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(@QueryParam("token") String token) {
        RestResponse<?> resp = new RestResponse<>();
        try {

            if( UAuthService.setInactiveAuth(token) > 0) {
                resp.setStatus("200");
                resp.setMessage("logout success");
                return Response.ok().entity(resp).build();
            } else {
                resp.setStatus("500");
                resp.setMessage("Can't check last auth as inactive");
                return Response.serverError().entity(resp).build();
            }
        } catch (UsersAuthentificationsDAOException e) {
            resp.setStatus("error");
            resp.setMessage(e.getMessage());
            return Response.serverError().entity(resp).build();
        }
    }

}