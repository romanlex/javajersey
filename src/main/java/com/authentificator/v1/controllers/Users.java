package com.authentificator.v1.controllers;

import com.authentificator.core.exceptions.UserDAOException;
import com.authentificator.v1.RestResponse;
import com.authentificator.v1.pojo.User;
import com.authentificator.v1.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/users")
public class Users {
    private static final Logger log = LoggerFactory.getLogger(Users.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public RestResponse<User> getUsers() {
        RestResponse<User> resp = new RestResponse<>();
        try {
            List<User> list = UserService.findAll();
            if(list.size() > 0) {
                resp.setStatus("200");
                resp.setData(list);
            } else {
                resp.setStatus("404");
                resp.setMessage("Users not found");
            }
        } catch (UserDAOException e) {
            e.printStackTrace();
        }
        return resp;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RestResponse<User> getUserById(@PathParam("id") int id) {
        RestResponse<User> resp = new RestResponse<>();
        try {
            User user = UserService.getUserById(id);
            if (user.getId() == 0) {
                resp.setStatus("404");
                resp.setMessage("User not found");
                resp.setData(new ArrayList<>());
            } else {
                resp.setStatus("200");
                List<User> users = new ArrayList<>();
                users.add(user);
                resp.setData(users);
            }
        } catch (UserDAOException e) {
            resp.setStatus("500");
            resp.setMessage(e.getMessage());
        }

        return resp;
    }

}