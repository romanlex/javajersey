package com.authentificator.core.database;

import com.authentificator.core.common.ObjectPool;
import com.authentificator.core.common.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnectionPool extends ObjectPool<Connection> {
    private static final Logger log = LoggerFactory.getLogger(JDBCConnectionPool.class);
    private String dsn, usr, pwd;

    public JDBCConnectionPool(String driver, String dsn, String usr, String pwd) {
        super();
        try {
            Class.forName(driver).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.expirationTime = Long.parseLong(Settings.getSettings(true).getProperty("database.pool.expirationTime"));
        this.dsn = dsn;
        this.usr = usr;
        this.pwd = pwd;
    }

    @Override
    protected Connection create() {
        try {
            return (DriverManager.getConnection(dsn, usr, pwd));
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            return (null);
        }
    }

    @Override
    public void expire(Connection o) {
        try {
            ((Connection) o).close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
        }
    }

    @Override
    public boolean validate(Connection o) {
        try {
            return (!((Connection) o).isClosed());
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            return (false);
        }
    }
}
