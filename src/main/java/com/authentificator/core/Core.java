package com.authentificator.core;

import com.authentificator.core.common.Settings;
import com.authentificator.core.database.Database;
import com.authentificator.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

public class Core {
    public static final Core INSTANCE = new Core();
    private static final Logger log = LoggerFactory.getLogger(Core.class);

    /**
     * Load config file, connect to databse
     * @throws CoreException Ex. cannot connect to database or parsing error config file
     */
    public void load() throws CoreException {
        log.info("Initialize com.authentificator.core of application", Core.class);
        try {
            log.info("Trying parse config file...", Core.class);
            Settings.readConfig();
        } catch (FileNotFoundException e) {
            throw new CoreException("Error when trying read config or load properties: " + e.getMessage());
        }

        if(Settings.getSettings().size() != 0) {
            Database.dbholder.getInstance().getConnection();
            log.info("Successfully connect to database.", Core.class);
        }
    }

}
