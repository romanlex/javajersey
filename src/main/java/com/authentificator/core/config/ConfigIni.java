package com.authentificator.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class ConfigIni {
    private static final Logger log = LoggerFactory.getLogger(ConfigIni.class);
    private Properties props = new Properties();
    private File file;

    public ConfigIni(File file) {
        this.file = file;
    }

    public Properties parseConfigFile() {
        try(InputStreamReader is = new InputStreamReader(new FileInputStream(file));
            BufferedReader br = new BufferedReader(is)) {
            String line;
            while ((line = br.readLine()) != null) {
                StringReader str = new StringReader(line);
                props.load(str);
            }
        } catch (IOException e) {
            log.error("Cannot read config file: {}", e.getMessage());
        }

        return props;
    }
}
