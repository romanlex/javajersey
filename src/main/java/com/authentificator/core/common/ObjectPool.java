package com.authentificator.core.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

public abstract class ObjectPool<T> {
    private static final Logger log = LoggerFactory.getLogger(ObjectPool.class);
    public long expirationTime = 30000;
    private ConcurrentHashMap<T, Long> locked, unlocked;

    public ObjectPool() {
        locked = new ConcurrentHashMap<T, Long>();
        unlocked = new ConcurrentHashMap<T, Long>();
    }

    protected abstract T create();
    public abstract boolean validate(T o);
    public abstract void expire(T o);

    public synchronized T checkOut() {
        long now = System.currentTimeMillis();
        T t;
        if (unlocked.size() > 0) {
            Enumeration<T> e = unlocked.keys();
            while (e.hasMoreElements()) {
                t = e.nextElement();
                if ((now - unlocked.get(t)) > expirationTime) {
                    // объект устарел
                    unlocked.remove(t);
                    expire(t);
                    t = null;
                } else {
                    if (validate(t)) {
                        unlocked.remove(t);
                        locked.put(t, now);
                        return (t);
                    } else {
                        // проверка объекта, не валиден(соединение закрыто)
                        unlocked.remove(t);
                        expire(t);
                        t = null;
                    }
                }
            }
        }
        // нет объектов, создаем новый
        t = create();
        locked.put(t, now);
        return (t);
    }

    public synchronized void checkIn(T t) {
        locked.remove(t);
        unlocked.put(t, System.currentTimeMillis());
    }

    public ConcurrentHashMap<T, Long> getUnlocked() {
        return unlocked;
    }

    public ConcurrentHashMap<T,Long> getLocked() {
        return locked;
    }
}
