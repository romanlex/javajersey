package com.authentificator.core.common;

import com.authentificator.core.Core;
import com.authentificator.core.config.ConfigIni;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Properties;

public class Settings extends Properties {
    public static final Settings INSTANCE = new Settings();
    private static final Logger log = LoggerFactory.getLogger(Core.class);
    private static Properties settings = new Properties();

    public static Properties getSettings() {
        return settings;
    }

    public static Properties getSettings(boolean parse) {
        if(parse) {
            try {
                readConfig();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return settings;
    }

    public static void setSettings(Properties settings) {
        Settings.settings = settings;
    }

    /**
     * Start parse config file
     * @throws FileNotFoundException config file not found
     */
    public static void readConfig() throws FileNotFoundException {
        ClassLoader classLoader = Settings.class.getClassLoader();
        File file = new File(classLoader.getResource("config.ini").getFile());
        ConfigIni config = new ConfigIni(file);
        Properties props = config.parseConfigFile();
        if(props.size() == 0) {
            throw new FileNotFoundException("Properties not setted because cannot read config file.");
        }

        log.info("Config file parsing successfully. Setting loaded.", Core.class);
        setSettings(props);
    }
}
