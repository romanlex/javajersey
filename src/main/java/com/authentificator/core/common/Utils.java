package com.authentificator.core.common;

import com.authentificator.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    /**
     * Return MD5 string
     * @param str get md5 hash string by string
     * @return String
     */
    public static String getMd5String(String str) throws CoreException {
        String code = null;
        try {
            byte[] bytesOfMessage = str.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] md5 = md.digest(bytesOfMessage);
            StringBuilder sb = new StringBuilder();
            for (byte aMd5 : md5) {
                sb.append(Integer.toHexString((aMd5 & 0xFF) | 0x100).substring(1, 3));
            }
            code = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(),e.getCause());
            throw new CoreException("Не получилось получить алгоритм шифрования. Обратитесь к администрации для решения проблемы.");
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(),e.getCause());
        }
        return code;
    }
}
