package com.authentificator.core.listeners;

import com.authentificator.core.Core;
import com.authentificator.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationLoadListener implements ServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(ApplicationLoadListener.class);
    ServletContext context;

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        System.out.println("Context creating ... ");
        try {
            Core core = new Core();
            core.load();
            ServletContext sc = contextEvent.getServletContext();
            sc.setAttribute("ctx", sc.getContextPath());

        } catch (CoreException e) {
            log.error(e.getMessage(), ApplicationLoadListener.class);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        System.out.println("Context Destroyed");
    }
}
