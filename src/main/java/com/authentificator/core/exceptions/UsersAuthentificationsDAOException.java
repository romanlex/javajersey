package com.authentificator.core.exceptions;

public class UsersAuthentificationsDAOException extends Exception {
    public UsersAuthentificationsDAOException(String s) {
        super(s);
    }

}
