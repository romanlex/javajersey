package com.authentificator.core.exceptions;

/**
 * Created by roman on 17.02.17.
 */
public class ModelException extends Exception {
    //Parameterless Constructor
    public ModelException() {}

    //Constructor that accepts a message
    public ModelException(String message)
    {
        super(message);
    }
}
