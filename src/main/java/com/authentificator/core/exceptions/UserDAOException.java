package com.authentificator.core.exceptions;

public class UserDAOException extends Exception {
    public UserDAOException(String s) {
        super(s);
    }

}
