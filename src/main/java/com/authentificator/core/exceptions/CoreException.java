package com.authentificator.core.exceptions;

/**
 * Created by roman on 17.02.17.
 */
public class CoreException extends Exception {
    //Parameterless Constructor
    public CoreException() {}

    //Constructor that accepts a message
    public CoreException(String message)
    {
        super(message);
    }
}
