Методы REST
-

* /api/v1/login - принимает логин и пароль методом POST, отдаёт токен
* /api/v1/logout - принимает токен в качестве GET параметра
* /api/v1/users - отдаёт список юзеров

В директории **sql-dump** дамп БД


Запрос на ```/api/v1/login```

![запрос на /api/v1/login](http://test-git.ic-i.ru/Olin/authentificator/raw/master/screenshots/Screenshot_20170330_211411.png)

Запрос на ```/api/v1/logout```

![запрос на /api/v1/login](http://test-git.ic-i.ru/Olin/authentificator/raw/master/screenshots/Screenshot_20170330_211420.png)


Запрос на ```/api/v1/users```

![запрос на /api/v1/login](http://test-git.ic-i.ru/Olin/authentificator/raw/master/screenshots/Screenshot_20170330_211428.png)

